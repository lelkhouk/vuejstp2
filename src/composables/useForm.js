import {ref} from  'vue';
import Schema from 'async-validator';

export function useForm(options){
    //const {} = useForm({options})
 
    const formErrors = ref({})
    const form = ref(options.form);
    //{
    //     email: '',
    //     password: '',
    //     confirmPassword: ''
    // }

    const validatorValues = ref(options.validatorValues);

    const validator = new Schema(validatorValues.value);


    return {
        form,
        validator,
        validatorValues,
        formErrors
    }

}